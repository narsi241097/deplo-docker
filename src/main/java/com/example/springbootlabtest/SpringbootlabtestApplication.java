package com.example.springbootlabtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootlabtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootlabtestApplication.class, args);
	}

}
